package com.example.application;



import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

public class adapterfor_child extends RecyclerView.Adapter<adapterfor_child.adapterforchildholder> {
    public Context context;
    public List<inside_category> category;

    public adapterfor_child(Context context, List<inside_category> category) {
        this.context = context;
        this.category = category;
    }

    @NonNull
    @Override
    public adapterforchildholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.image_card_view,null);
        return new adapterfor_child.adapterforchildholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull adapterforchildholder holder, final int position) {
        final inside_category cat =category.get(position);
        holder.textname.setText(cat.catChildName);
        Glide.with(context).load(category.get(position).getcatChildUrl())
                .into(holder.imageView);
        final int id= category.get(position).getcatId();
        holder.cw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context,"youclicked"+category.get(position).getcatChildName(),Toast.LENGTH_SHORT).show();
                Intent intent= new Intent(context,after_child_category.class);
                intent.putExtra("catid",category.get(position).getcatId());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });



    }

    @Override
    public int getItemCount() {
        return category.size();
    }

    public class adapterforchildholder extends RecyclerView.ViewHolder {
        TextView textname;
        ImageView imageView;
        CardView cw;

        public adapterforchildholder(@NonNull View itemView) {
            super(itemView);
            textname = itemView.findViewById(R.id.name_tv);
            imageView = itemView.findViewById(R.id.image_v);
            cw = itemView.findViewById(R.id.card_view_image);


        }
    }
}
