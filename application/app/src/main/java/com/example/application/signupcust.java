package com.example.application;

import androidx.appcompat.app.AppCompatActivity;
import com.example.application.R;
import com.google.android.material.textfield.TextInputLayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class signupcust extends AppCompatActivity implements View.OnClickListener {

    Button b1;
    TextView t1;
    TextInputLayout tf1,tf2,tf3,tf4;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signupcust);
        b1=(Button)findViewById(R.id.button_login_cust);
        t1=(TextView)findViewById(R.id.signin_log);
        tf1=(TextInputLayout)findViewById(R.id.cust_name);
        tf2=(TextInputLayout)findViewById(R.id.cust_number);
        tf3=(TextInputLayout)findViewById(R.id.cust_password);
        tf4=(TextInputLayout)findViewById(R.id.cust_confirm_pass);
        b1.setOnClickListener(this);
        t1.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.button_login_cust)
        {


            //            Toast.makeText(this,"login from cutomer clicked",Toast.LENGTH_LONG).show();
            String name = tf1.getEditText().getText().toString();
            String mobile_number = tf2.getEditText().getText().toString();
            String password = tf3.getEditText().getText().toString();
            String user_type="1";
            String confirm_password = tf4.getEditText().getText().toString();

            if(name.equals(null) ||mobile_number.equals(null)||password.equals(null) ||! password.equals(confirm_password) || password.length()<=6) {

                Toast.makeText(getApplicationContext(),"please fillout all the fields and try again later or check the password",Toast.LENGTH_LONG).show();

            }
            else {
                Call<ResponseBody> call = RetrofitClient
                        .getInstance()
                        .getApi()
                        .createUser(name, mobile_number, user_type, password);

                call.enqueue(new Callback<ResponseBody>() {
                    String s;

                    @Override

                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        try {
                            s = response.body().string();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        if(response.isSuccessful()) {
                            Toast.makeText(getApplicationContext(), "sucessfuly regiterd"+response.code(), Toast.LENGTH_LONG).show();

                        }



                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();

                    }
                });
//                tf1.clear();
//                tf2.setText("");
//                tf3.setText("");
//                tf4.setText("");
                Intent i = new Intent(getApplicationContext(),MainActivity.class);
                startActivity(i);


            }






        }
        if(v.getId()==R.id.signin_log)
        {
            Intent intent = new Intent(getApplicationContext(),MainActivity.class);
            startActivity(intent);

        }

    }
}
