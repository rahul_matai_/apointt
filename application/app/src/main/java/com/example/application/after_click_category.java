package com.example.application;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class after_click_category extends AppCompatActivity {

    RecyclerView rcv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_after_click_category);
        rcv=(RecyclerView)findViewById(R.id.rcv_inside_category);
        Intent intent= getIntent();
        String catName= intent.getStringExtra("catName");
        rcv.setLayoutManager(new GridLayoutManager(getApplicationContext(),2));



        Call<List<inside_category>> call =RetrofitClient.getInstance().getApi().getCategory(catName);
        call.enqueue(new Callback<List<inside_category>>() {
            @Override
            public void onResponse(Call<List<inside_category>> call, Response<List<inside_category>> response) {
                if(response.isSuccessful())
                {


                    rcv.setAdapter(new adapterfor_child(getApplicationContext(),response.body()));
                    Toast.makeText(getApplicationContext(),"Success",Toast.LENGTH_LONG).show();
                }


            }

            @Override
            public void onFailure(Call<List<inside_category>> call, Throwable t) {

            }
        });


    }
}
