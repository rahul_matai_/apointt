package com.example.application;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class second_page extends AppCompatActivity implements View.OnClickListener{

    Button b1,b2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second_page);
        b1=(Button)findViewById(R.id.Customer_signup_btn);
        b2=(Button)findViewById(R.id.business_signup_btn);
        b1.setOnClickListener(this);
        b2.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.Customer_signup_btn)
        {
            Intent intent = new Intent(getApplicationContext(),signupcust.class);
            startActivity(intent);
        }
        if(v.getId()==R.id.business_signup_btn)
        {
            Intent intent = new Intent(getApplicationContext(),signupbusiness.class);
            startActivity(intent);
        }

    }
}
