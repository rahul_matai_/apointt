package com.example.application;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

public class adapterfor_finalpage extends RecyclerView.Adapter<adapterfor_finalpage.adapter_holder>{
    public Context context;
    public List<inside_child> child;

    public adapterfor_finalpage(Context context, List<inside_child> child) {
        this.context = context;
        this.child = child;
    }

    @NonNull
    @Override
    public adapter_holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.final_card,null);

//        name.setText(items.get(position).getCatName());
//        Glide.with(context).load(items.get(position).getCatUrl())
//                .into(imageView);

        return new adapter_holder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull adapter_holder holder, int position) {
        inside_child is= child.get(position);
        holder.t1.setText(is.getprovider_name().toString());
        holder.t2.setText(is.getqualification().toString());
        holder.t3.setText(is.getcity().toString());
        holder.t4.setText(is.getcost()+"");
        Glide.with(context).load(child.get(position).getprovider_url())
                .into(holder.iv);



    }

    @Override
    public int getItemCount() {
        return child.size();
    }


    public static class adapter_holder extends RecyclerView.ViewHolder  {
        ImageView iv;
        TextView t1,t2,t3,t4,t5;
        Button b1,b2,b3;
        public adapter_holder(@NonNull View itemView) {
            super(itemView);
            iv=(ImageView)itemView.findViewById(R.id.child_profile_picture);
            t1=(TextView)itemView.findViewById(R.id.child_name);
            t2=(TextView)itemView.findViewById(R.id.child_qualification);
            t3=(TextView)itemView.findViewById(R.id.child_city);
            t4=(TextView)itemView.findViewById(R.id.child_money);
            t5=(TextView)itemView.findViewById(R.id.child_appoint);
            b1=(Button)itemView.findViewById(R.id.child_view_profile);
            b2=(Button)itemView.findViewById(R.id.child_contact);
            b3=(Button)itemView.findViewById(R.id.child_appointment);


        }
    }
}
