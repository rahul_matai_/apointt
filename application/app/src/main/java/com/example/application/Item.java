package com.example.application;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Item {

    @SerializedName("catId")
    @Expose
    private Integer catId;
    @SerializedName("catName")
    @Expose
    private String catName;
    @SerializedName("catUrl")
    @Expose
    private String catUrl;
    @SerializedName("catLevel")
    @Expose
    private Integer catLevel;
    @SerializedName("catParent")
    @Expose
    private Object catParent;
    @SerializedName("catDesc")
    @Expose
    private String catDesc;
    @SerializedName("catCount")
    @Expose
    private Integer catCount;
    @SerializedName("catBgUrl")
    @Expose
    private String catBgUrl;

    public Integer getCatId() {
        return catId;
    }

    public void setCatId(Integer catId) {
        this.catId = catId;
    }

    public Item withCatId(Integer catId) {
        this.catId = catId;
        return this;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public Item withCatName(String catName) {
        this.catName = catName;
        return this;
    }

    public String getCatUrl() {
        return catUrl;
    }

    public void setCatUrl(String catUrl) {
        this.catUrl = catUrl;
    }

    public Item withCatUrl(String catUrl) {
        this.catUrl = catUrl;
        return this;
    }

    public Integer getCatLevel() {
        return catLevel;
    }

    public void setCatLevel(Integer catLevel) {
        this.catLevel = catLevel;
    }

    public Item withCatLevel(Integer catLevel) {
        this.catLevel = catLevel;
        return this;
    }

    public Object getCatParent() {
        return catParent;
    }

    public void setCatParent(Object catParent) {
        this.catParent = catParent;
    }

    public Item withCatParent(Object catParent) {
        this.catParent = catParent;
        return this;
    }

    public String getCatDesc() {
        return catDesc;
    }

    public void setCatDesc(String catDesc) {
        this.catDesc = catDesc;
    }

    public Item withCatDesc(String catDesc) {
        this.catDesc = catDesc;
        return this;
    }

    public Integer getCatCount() {
        return catCount;
    }

    public void setCatCount(Integer catCount) {
        this.catCount = catCount;
    }

    public Item withCatCount(Integer catCount) {
        this.catCount = catCount;
        return this;
    }

    public String getCatBgUrl() {
        return catBgUrl;
    }

    public void setCatBgUrl(String catBgUrl) {
        this.catBgUrl = catBgUrl;
    }

    public Item withCatBgUrl(String catBgUrl) {
        this.catBgUrl = catBgUrl;
        return this;
    }

}