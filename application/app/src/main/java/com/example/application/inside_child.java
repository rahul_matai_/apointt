package com.example.application;

class inside_child {

    long provider_id;
    String provider_name;
    String provider_url;
    String provider_mobile_no;
    String qualification;
    String day;
    String startTime;
    String endTime;
    String city;
    //    double lat;
//    double long;
    int cost;
    String address;
    int experience;
    String fav_status;
    String gender;
    long providerUserId;

    public inside_child(long provider_id, String provider_name, String provider_url, String provider_mobile_no, String qualification, String day, String startTime, String endTime, String city, int cost, String address, int experience, String fav_status, String gender, long providerUserId) {
        this.provider_id = provider_id;
        this.provider_name = provider_name;
        this.provider_url = provider_url;
        this.provider_mobile_no = provider_mobile_no;
        this.qualification = qualification;
        this.day = day;
        this.startTime = startTime;
        this.endTime = endTime;
        this.city = city;
        this.cost = cost;
        this.address = address;
        this.experience = experience;
        this.fav_status = fav_status;
        this.gender = gender;
        this.providerUserId = providerUserId;
    }

    public long getprovider_id() {
        return provider_id;
    }

    public String getprovider_name() {
        return provider_name;
    }

    public String getprovider_url() {
        return provider_url;
    }

    public String getprovider_mobile_no() {
        return provider_mobile_no;
    }

    public String getqualification() {
        return qualification;
    }

    public String getday() {
        return day;
    }

    public String getstartTime() {
        return startTime;
    }

    public String getendtime() {
        return endTime;
    }

    public String getcity() {
        return city;
    }

    public int getcost() {
        return cost;
    }

    public String getaddress() {
        return address;
    }

    public int getexperience() {
        return experience;
    }

    public String getfav_status() {
        return fav_status;
    }

    public String getgender() {
        return gender;
    }

    public long getproviderUserId() {
        return providerUserId;
    }
}
