package com.example.application;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPrefManager {
    private static String SHARED_PREF_NAME = "user";
    private static  SharedPrefManager mInstance;
    private Context ctx;

    public SharedPrefManager(Context ctx) {
        this.ctx=ctx;

    }
    public static  synchronized  SharedPrefManager getInstance(Context mctx)
    {
        if(mInstance==null)
        {
            mInstance= new SharedPrefManager(mctx);
        }
        return mInstance;
    }

    public void saveUser(User u)
    {
        SharedPreferences sharedPreferences = ctx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        SharedPreferences.Editor  editor = sharedPreferences.edit();
        editor.putString("id",u.getUser_id());
        editor.putString("name",u.getName());
        editor.putString("provider_id",u.getProvider_id());
        editor.apply();
    }
    public boolean isLoggedIn(){
        SharedPreferences sharedPreferences = ctx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        if(sharedPreferences.getString("id",null) != null)
        {
            return true;

        }
        else
        {
            return false;
        }
    }
    public User getUser()
    {
        SharedPreferences sharedPreferences = ctx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        User user = new User(sharedPreferences.getString("id",null),
                sharedPreferences.getString("name",null),
                sharedPreferences.getString("provider_id",null)
        );
        return user;
    }
    public void clear(){
        SharedPreferences sharedPreferences = ctx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        SharedPreferences.Editor  editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();


    }
}
