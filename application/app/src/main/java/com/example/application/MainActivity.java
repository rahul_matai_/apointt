package com.example.application;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
TextInputLayout tf1,tf2;
Button b1;
RadioButton rb1,rb2;
TextView ed1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tf1=(TextInputLayout)findViewById(R.id.phonenumber);
        tf2=(TextInputLayout)findViewById(R.id.password);
        b1=(Button)findViewById(R.id.button);
        rb1=(RadioButton)findViewById(R.id.iscustomer);
        rb2=(RadioButton)findViewById(R.id.isbusiness);
        ed1=(TextView) findViewById(R.id.login_login);
        b1.setOnClickListener(this);
        rb1.setOnClickListener(this);
        rb2.setOnClickListener(this);
        ed1.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        if(v.getId()==R.id.button)
        {
//            if(!user_empty() || !pass_empty()){
//               return;
//
//            }
//            else{
//                Toast.makeText(getApplicationContext(),"phone and password are",Toast.LENGTH_LONG).show();
//            }

            String mobile_number =tf1.getEditText().getText().toString();
            String password = tf2.getEditText().getText().toString();
            Call<User> call = RetrofitClient.getInstance()
                    .getApi().userLogin(mobile_number,password);
            call.enqueue(new Callback<User>() {
                @Override
                public void onResponse(Call<User> call, Response<User> response) {
                    if (response.isSuccessful()) {
                        Toast.makeText(getApplicationContext(), "success", Toast.LENGTH_LONG).show();
                        SharedPrefManager.getInstance(getApplicationContext())
                                .saveUser(response.body());
                        Intent i = new Intent(getApplicationContext(), after_login.class);
//                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                        startActivity(i);

                    }


                }

                @Override
                public void onFailure(Call<User> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), "failed" + t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });

        }
        if(v.getId()==R.id.login_login)
        {
            Intent intent = new Intent(getApplicationContext(),second_page.class);
            startActivity(intent);
        }


    }
    private boolean user_empty()
    {
        String phone= tf1.getEditText().getText().toString().trim();
        if(phone.isEmpty()){
            tf1.setError("phone Number is mandatory");
            return true;
        }
        else{
            tf1.setError(null);
            return false;
        }
    }
    private boolean pass_empty()
    {
        String pass= tf2.getEditText().getText().toString().trim();
        if(pass.isEmpty()){
            tf2.setError("password Number is mandatory and should be greater than 6");
            return true;
        }
        else{
            tf2.setError(null);
            return false;
        }
    }

}
