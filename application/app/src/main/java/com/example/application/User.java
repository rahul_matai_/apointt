package com.example.application;

public class User{
    String name;
    String user_id;
    String provider_id;

    public User(String name, String user_id, String provider_id) {
        this.name = name;
        this.user_id = user_id;
        this.provider_id = provider_id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public void setProvider_id(String provider_id) {
        this.provider_id = provider_id;
    }

    public String getName() {
        return name;
    }

    public String getUser_id() {
        return user_id;
    }

    public String getProvider_id() {
        return provider_id;
    }
}
