package com.example.application;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {
    private static  final  String  Base_URL="http://ec2-3-16-91-238.us-east-2.compute.amazonaws.com:8081/";
    private static RetrofitClient mInstance;
    private Retrofit retrofit;

    private RetrofitClient(){
        retrofit = new Retrofit.Builder()
                .baseUrl(Base_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }
    //    for single instance
    public static synchronized RetrofitClient getInstance(){
        if(mInstance == null)
        {
            mInstance = new RetrofitClient();
        }
        return mInstance;

    }
    public jsonPlaceHolder getApi(){
        return retrofit.create(jsonPlaceHolder.class);

    }
}
