package com.example.application;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class after_login extends AppCompatActivity {

    RecyclerView rcv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_after_login);
        rcv=(RecyclerView)findViewById(R.id.rcv);
        rcv.setLayoutManager(new GridLayoutManager(getApplicationContext(),2));


        Call<List<Item>> call =RetrofitClient.getInstance().getApi().getItems();
        call.enqueue(new Callback<List<Item>>() {
            @Override
            public void onResponse(Call<List<Item>> call, Response<List<Item>> response) {

                if(response.isSuccessful())
                {


                    rcv.setAdapter(new Custom_adapter(getApplicationContext(),response.body()));
                    Toast.makeText(getApplicationContext(),"Success",Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<List<Item>> call, Throwable t) {

                Toast.makeText(getApplicationContext(),"Error occured",Toast.LENGTH_LONG).show();
                Log.e("ERROR OCCURED",t.getMessage());


            }
        });



    }
}
