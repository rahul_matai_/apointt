package com.example.application;

public class inside_category {
    int catId;
    String catChildName;
    int catChildId;
    String catChildUrl;
    int catChildLevel;
    String catChildDesc;
    int providerCount;

    public inside_category(int catId, String catChildName, int catChildId, String catChildUrl, int catChildLevel, String catChildDesc, int providerCount) {
        this.catId = catId;
        this.catChildName = catChildName;
        this.catChildId = catChildId;
        this.catChildUrl = catChildUrl;
        this.catChildLevel = catChildLevel;
        this.catChildDesc = catChildDesc;
        this.providerCount = providerCount;
    }

    public void setcatId(int catId) {
        this.catId = catId;
    }

    public void setcatChildName(String catChildName) {
        this.catChildName = catChildName;
    }

    public void setcatChildId(int catChildId) {
        this.catChildId = catChildId;
    }

    public void setcatChildUrl(String catChildUrl) {
        this.catChildUrl = catChildUrl;
    }

    public void setcatChildLevel(int catChildLevel) {
        this.catChildLevel = catChildLevel;
    }

    public void setcatChildDesc(String catChildDesc) {
        this.catChildDesc = catChildDesc;
    }

    public void setproviderCount(int providerCount) {
        this.providerCount = providerCount;
    }

    public int getcatId() {
        return catId;
    }

    public String getcatChildName() {
        return catChildName;
    }

    public int getcatChildId() {
        return catChildId;
    }

    public String getcatChildUrl() {
        return catChildUrl;
    }

    public int getcatChildLevel() {
        return catChildLevel;
    }

    public String getcatChildDesc() {
        return catChildDesc;
    }

    public int getproviderCount() {
        return providerCount;
    }
}