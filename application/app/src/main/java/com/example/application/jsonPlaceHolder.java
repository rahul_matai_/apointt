package com.example.application;
import com.example.application.User;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

public interface jsonPlaceHolder {
    @FormUrlEncoded
    @POST("users/signupProviderWithPromise")
    Call<ResponseBody> createBusinessuser(
            @Field("business_name") String business_name,
            @Field("mobile_number") String mobile_number,
            @Field("user_type") String user_type,
            @Field("password") String password);

    @FormUrlEncoded
    @POST("users/signUpApointt")
    Call<ResponseBody> createUser(
            @Field("name") String business_name,
            @Field("mobile_number") String mobile_number,
            @Field("user_type") String user_type,
            @Field("password") String password);

    @FormUrlEncoded
    @POST("users/logInApointt")
    Call<User> userLogin(
            @Field("mobile_number") String mobile_number,
            @Field("password") String password
    );

    @GET("category/getCategory")
    Call<List<Item>> getItems();

    @GET("category/getChildCategory")
    Call<List<inside_category>> getCategory(@Query("category_name") String catName);

    @GET("providers/getProvider")
    Call<List<inside_child>> getChild(@Query("category_id")int catid);


}
