package com.example.application;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

public class Custom_adapter extends RecyclerView.Adapter<Custom_adapter.customViewHolder> {

    public Context context;
    public List<Item> items;

    public Custom_adapter(Context context, List<Item> items) {
        this.context = context;
        this.items = items;
    }

    @NonNull
    @Override
    public customViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.image_card_view,null);

//        name.setText(items.get(position).getCatName());
//        Glide.with(context).load(items.get(position).getCatUrl())
//                .into(imageView);

        return new customViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final customViewHolder holder, final int position) {
        Item item= items.get(position);
        holder.textname.setText(item.getCatName());
        Glide.with(context).load(items.get(position).getCatUrl())
                .into(holder.imageView);
        final String itemsss= holder.textname.getText().toString();
        holder.cw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context,"you clicked" +" "+items.get(position).getCatName(),Toast.LENGTH_LONG).show();
                Intent i = new Intent(context,after_click_category.class);
                i.putExtra("catName",items.get(position).getCatName());
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class customViewHolder extends RecyclerView.ViewHolder{
        TextView textname;
        ImageView imageView;
        CardView cw;
        public customViewHolder(@NonNull View itemView) {
            super(itemView);
            textname = itemView.findViewById(R.id.name_tv);
            imageView = itemView.findViewById(R.id.image_v);
            cw=itemView.findViewById(R.id.card_view_image);


        }
    }
}
