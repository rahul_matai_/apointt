package com.example.application;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class after_child_category extends AppCompatActivity {

    RecyclerView rc;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_after_child_category);
        Intent intent= getIntent();
        final int id = intent.getIntExtra("catid",-1);
        rc=(RecyclerView)findViewById(R.id.rcv_inside_categorychild);
        rc.setLayoutManager(new GridLayoutManager(getApplicationContext(),1));

        Call<List<inside_child>> call = RetrofitClient.getInstance().getApi().getChild(id);
        call.enqueue(new Callback<List<inside_child>>() {
            @Override
            public void onResponse(Call<List<inside_child>> call, Response<List<inside_child>> response) {
                String Content="";
                List<inside_child> insc = response.body();

                if(response.isSuccessful()){


                    rc.setAdapter(new adapterfor_finalpage(getApplicationContext(),response.body()));




                }


            }

            @Override
            public void onFailure(Call<List<inside_child>> call, Throwable t) {

                Toast.makeText(getApplicationContext(),"ERROR"+t.getMessage(),Toast.LENGTH_LONG).show();

            }
        });


    }
}
